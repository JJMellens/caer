# Tests

These files are a lot messier and more poorly documented than the rest of the repo, as they are not an official part of the main program but instead serve as my personal way to test the client-side functionality. Why even keep them in this repo you ask? Good question, the answer is I haven't bothered cleaning it up since I first made it.


### network_test_cases.py

This goes through all the intended client-side functionality once and returns the amount of successes. I'll have to update it with comments some day.

### demo.py

This is a modified version of the previous file that I use in the demonstration video.
