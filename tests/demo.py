import socket
import sys
import asyncio
import ssl
import websockets
import pathlib
import json
import time
import traceback
import os
from Crypto.Cipher import AES
from core.settings import settings
from core.security import security
from core.whitelist import whitelist
from core.portforwards import forwarded_ports
from core.devices import devices
from client.helpers import client_connect, accept_dh_exchange, send_encrypted, get_debug_request, send_2fa_message
from helpers import receive_and_decrypt
import data.statuscodes as c
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, ParameterFormat, load_pem_public_key, load_pem_parameters

async def test_client_stub():


    success = 0
    failure = 0
    # 2FA
    server_ip = "192.168.1.16"
    server_port = int(settings.resource["2fa_port"])
    twofa_address = (server_ip, server_port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection = client_connect(sock, twofa_address)
    try:
        print("Sending a 2FA message")
        reply = send_2fa_message(connection) 
        print("SUCCES! Server returned:\n\t\t{}".format(reply))
        success +=1
    except Exception as err:
        return("{} in PUT: {}".format(type(err).__name__, traceback.format_exc()), None)
        failure +=1

        print(("FAILURE: server did not respond"))
    finally:
        print('\n')
        sock.close()
        time.sleep(.2)

    #Local
    server_address = "ws://192.168.1.16:12340"
    async with websockets.connect(server_address) as websocket:


        # iptables -I INPUT -s 192.168.1.13 -j ACCEPT





        test_cases = {
            #"GET the list of devices": ["known_devices", None, "GET", None]
            #"PUT laptop into IoT LAN" : ["known_devices","48E244B8965F", "PUT", {"devicetype":"IOT", "name":"laptop"}]      
            #"GET port forward rules" : ["forwarded_ports", None, "GET", None]
            #"POST rule" : ["forwarded_ports","demo","POST",{"start_port":"12000","end_port":"12000","protocols":["TCP", "UDP"],"active":True,"destination_ip":"10.10.82.76"}]
            #"GET port forward rules" : ["forwarded_ports", None, "GET", None]
            #"GET the AES key" : ["security", "aes_key", "GET", None]
            }







        """
        test_cases = {
            "2: GET list of settings" : 
                ["settings", None, "GET", None],
            "3: GET setup_completed from settings" : 
                ["settings", "setup_completed", "GET", None],
            "4: PUT a new value to setup_completed" :
                ["settings","setup_completed", "PUT",True],
            "5: GET the list of devices":
                ["known_devices", None, "GET", None],
            "6: PUT POCO_LOCO into another LAN" :
                ["known_devices","B02A43091759", "PUT", {"devicetype":"REGULAR"}],
            "7: POST a new device" : 
                ["known_devices", "ASASDASDA","POST", 
                {"name":"TEST_DEVICE", "mac":"A6:50:46:0E:BF:E3", "devicetype":"REGULAR", "ip":"10.0.0.10","subnet":"NaN"}],
            "8: DEL the newly created device" : 
                ["known_devices", "ASASDASDA" , "DEL", None],
            "9: GET the AES key" : 
                ["security", "aes_key", "GET", None],
            "10: POST a deactivated  port forward rule" : 
                ["forwarded_ports","test","POST",
                 {"start_port":"12000","end_port":"12000","protocols":["TCP", "UDP"],"active":True,"destination_ip":"10.10.2.20"}],
            "11: GET port forward rules" : 
                ["forwarded_ports", None, "GET", None],
            "12: GET a specific port forward rule": 
                ["forwarded_ports" , "test", "GET", None],
            "13: PUT (activate) the new port forward rule" : 
                ["forwarded_ports", "test", "PUT", 
                 {"start_port":"80","end_port":"80","protocols":["TCP","UDP"],"active":True,"destination_ip":"1.1.1.1"}],
            "14: DEL a new port forward rule" : 
                ["forwarded_ports", "test", "DEL", None],
            "15: GET all whitelist rules": 
                ["whitelist" , None, "GET", None]
             }
        """
        key, session_id, token = await establish_session(websocket)
        auth_status = bytearray([0x03])
        prefix = auth_status+session_id
        for i in test_cases:
            print("Test case {}".format(i))
            try:
                message = json.dumps(test_cases[i]).encode("utf-8")
                message = await encrypt_data(session_id+token+message, key)
                await websocket.send(prefix+message)
                reply = await websocket.recv()
                if reply[:1] == session_id:
                    reply_status = await decrypt_message(reply[1:], key)
                    reply_status = reply_status.decode("utf-8")
                    reply = await(websocket.recv())
                    if reply[:1] == session_id:
                        reply_content = await decrypt_message(reply[1:], key)
                        reply_content = reply_content.decode("utf-8")
                        if(reply_status == c.OK):
                            print("SUCCES! Server returned:\n\t\t{}".format(reply_content))
                            success+=1
                        else:
                            print("FAILURE: server returned:\n\t\t{}".format(reply_status))
                            failure+=1
            except Exception as err:
                return("{} in Test Case: {}".format(type(err).__name__, traceback.format_exc()), None)
            finally:
                print("\n")
            time.sleep(0)

        print("{}/{} test cases were succesful, {}/{} test cases failed.".format(success,len(test_cases),failure,len(test_cases)))
        if success == len(test_cases):
            print("\n\n\
##############################################################\n\
##############################################################\n\
######                                                  ######\n\
######    Congratulations, all tests were succesful!    ######\n\
######                                                  ######\n\
##############################################################\n\
##############################################################\n")
        return
    return

async def establish_session(websocket):
    await websocket.send(bytearray([0x00, 0x00])) # Request session_id and dh_params
    reply = await websocket.recv()
    session_id = reply[:1]
    dh_params = reply[1:]
    parameters = load_pem_parameters(dh_params, backend=default_backend())
    # Generate client key
    client_private_key = parameters.generate_private_key()
    # Serialize public key & send to server
    client_public_key_ser = client_private_key.public_key().public_bytes(Encoding.PEM, PublicFormat.SubjectPublicKeyInfo)
    auth_status = bytearray([0x01])
    await websocket.send(auth_status + session_id + client_public_key_ser)
    reply = await websocket.recv()
    if session_id == reply[:1]:
        server_public_key = load_pem_public_key(reply[1:], backend=default_backend())
        shared_key = client_private_key.exchange(server_public_key)
        derived_key = HKDF(algorithm=hashes.SHA256(), length=32, salt=None, info=b'handshake data', backend=default_backend()).derive(shared_key)
        auth_status = bytearray([0x02])
        await websocket.send(auth_status + session_id + await encrypt_data(session_id + b'courtsofwinter', derived_key))
        reply = await websocket.recv()
        if session_id == reply[:1]:
            token = reply[1:]
            return(derived_key, session_id, token)

async def decrypt_message(data, key):
    """Return a bytes object containing a decrypted message.

    Decrypts using symmetric encryption and the 2FA key.
    
    Arguments
    ---------
    data : bytes
        AES-encrypted message

    Returns
    -------
    bytes
        Decrypted message
    """
    nonce, tag, ciphertext = [i for i in (data[:16],data[16:32],data[32:])]
    cipher = AES.new(key, AES.MODE_EAX, nonce)
    message = cipher.decrypt_and_verify(ciphertext, tag)
    return(message)

async def encrypt_data(data, key):
    """Return an AES-encrypted message.

    Decrypts using symmetric encryption and the 2FA key.

    Arguments
    ---------
    data : bytes
        Unencrypted message

    Returns
    -------
    bytes
        AES-encrypted data
    """
    cipher = AES.new(key, AES.MODE_EAX, mac_len=16)
    ciphertext, tag = cipher.encrypt_and_digest(data)
    return(cipher.nonce + tag + ciphertext)

asyncio.get_event_loop().run_until_complete(test_client_stub()) == 0


