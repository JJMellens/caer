import traceback, yaml, socket, sys, os, time
from Crypto.Cipher import AES
from core.security import security


def create_socket(host_ip, host_port, service):
    """Create and return a TCP socket to listen on.

    TODO: Add cleanup function that attempts to remove old sockets
          if initial launch fails.
    
    Parameters
    ----------
    host_ip : string
        IP address to listen on
    host_port : integer
        Port to listen on
    service : string
        Brief description of the service calling the function

    Returns
    -------
    socket.socket
        Active socket listening for connections.
    """

    while True:
        try:            
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_address = (host_ip, host_port)
            print('starting up a {} server on {} port {}'.format(service, *server_address))
            sock.bind(server_address)
            return(sock)
        except:
            print("Failed to launch {} server, trying again in five seconds".format(service))
            time.sleep(5)
    return


def decrypt_data(data):
    """Return a bytes object containing a decrypted message.

    Decrypts using EAX-mode AES and the 2FA key.
    
    Arguments
    ---------
    data : bytes
        AES-encrypted message

    Returns
    -------
    bytes
        Decrypted message
    """
    nonce, tag, ciphertext = [i for i in (data[:16],data[16:32],data[32:])]
    cipher = AES.new(security.resource["aes_key"], AES.MODE_EAX, nonce)
    message = cipher.decrypt_and_verify(ciphertext, tag)
    return(message)


def encrypt_data(data):
    """Return an AES-encrypted message.

    Decrypts using EAX-mode AES and the 2FA key.

    Arguments
    ---------
    data : bytes
        Unencrypted message

    Returns
    -------
    bytes
        AES-encrypted data
    """
    cipher = AES.new(security.resource["aes_key"], AES.MODE_EAX, mac_len=16)
    ciphertext, tag = cipher.encrypt_and_digest(data)
    return(cipher.nonce + tag + ciphertext)


def receive_and_decrypt(connection):
    """Receive an encrypted bytes object, returning a bytes object containing the decrypted message.

    Arguments
    ---------
    connection : socket.socket
        Socket where the message arrived

    Returns
    -------
    bytes
        Unencrypted message
    """

    data = receive_data(connection)
    message = decrypt_data(data)
    return(message)


def receive_data(connection):
    """Receive and return an arbitrary amount of data over a TCP socket.
    
    Arguments
    ---------
    connection : socket.socket
        Active connection from where data is expected

    Returns
    -------
    bytes
        Data received from the socket
    """

    data_received = 0
    data = b''
    message_len = int.from_bytes(connection.recv(2), byteorder='big')
    while data_received < message_len:
        datablock = connection.recv(1024)
        if datablock:
            #print('received {!r}'.format(datablock))
            data += datablock
            data_received += len(datablock)
        else:
            break
    return(data)


def send_data(connection, message):
    """Send an arbitrary amount of data over a TCP socket, prefixing it with the message length in the first 8 bytes.
    
    Arguments
    ---------
    connection "socket.socket
        Active connection to send data over
    message : bytes
        Message to be sent
    """
    data = len(message).to_bytes(2, byteorder='big')+message
    connection.sendall(data)
    return


def send_encrypted(connection, message):
    """Unifying function for sending and encrypting data in a clean manner.

    Calls encrypt_data to encrypt the message followed by send_data to send it.
    
    Arguments
    ---------
    connection : socket.socket
        Active connection of which data is to be sent
    message : bytes
        Message to be sent

    """
    send_data(connection, encrypt_data(message))
    return
