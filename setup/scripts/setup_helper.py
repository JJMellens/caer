from subprocess import Popen
import os
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import ParameterFormat, Encoding

def initialize():
    """Automate initialisation of the device
    by building the requisite files.
    
    """
    #dhcpdleases()
    ifcfg()
    sec_cfg()
    Popen(['systemctl', 'restart', 'dhcpd'])

def ifcfg():
    """Ensure that the interface has all the required IP addresses.
    
    Retains device-unique information and defines the local gateway and 
    DNS servers, as well as the IP adresses for the R-LAN, quarantine-LAN, and
    IoT-LAN interfaces.
    """
    with open('/etc/sysconfig/network-scripts/ifcfg-Wired_connection_1', "r+") as f:
        filelines = f.readlines()
        f.seek(0)
        for i in filelines:
            if "IPADDR" not in i and "PREFIX" not in i\
            and "GATEWAY" not in i and "DNS" not in i:
                f.write(i)
        f.write("DNS1=1.1.1.1\n")
        f.write("DNS2=1.0.0.1\n")
        f.write("GATEWAY=10.0.0.1\n")
        f.write("IPADDR=10.0.0.10\n")
        f.write("PREFIX=8\n")
        f.write("IPADDR1=10.1.0.1\n")
        f.write("PREFIX1=16\n")
        f.write("IPADDR2=10.5.0.1\n")
        f.write("PREFIX2=16\n")
        for i in range(0,256):
            f.write("IPADDR{}=10.10.{}.1\n".format(i+3, i))
            f.write("PREFIX{}=24\n".format(i+3))
    Popen(['ifdown', 'eth0']).communicate()
    Popen(['ifup', 'eth0']).communicate()

def dhcpd_reset():
    """Empty the DHCP lease file."""
    if os.path.isfile('/var/lib/dhcpd/dhcpd.leases'):
        Popen(['rm', '/var/lib/dhcpd/dhcpd.leases'])
    Popen(['touch','/var/lib/dhcpd/dhcpd.leases'])

def sec_cfg():
    """Generate and write to disk the security parameters this device will use.
        
    Generates a 256-bit AES key, 2048-bit DH parameters, and a password plus its hash.
    Upon creation the password will be printed to screen so that it can be put on a sticker or
    something and the device can be sent to the customer.

    NOTE: Random password generation is disabled, re-enable this by uncommenting
    the 'get_random_bytes' line and removing the hardcoded 'password = greatwallsoffire' line,
    before moving to production.
    """

    with open('/caer/data/security.yaml', "w") as f:
            securitydict = {}
            securitydict["aes_key"] = get_random_bytes(32)
            securitydict["dh_parameters"] = dh.generate_parameters(generator=2,\
               key_size=2048, backend=default_backend()).parameter_bytes(Encoding.DER, ParameterFormat.PKCS3)
            password = 'greatwallsoffire'
            #password = get_random_bytes(7).hex)
            print("########\n########\nDevice-unique password: ",password,"\n########\n########")
            password = SHA256.new(data=password.encode('utf-8'))
            securitydict["hashed_password"] = password.digest()
            yaml.dump(securitydict, f)

initialize()
