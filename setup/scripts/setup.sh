# Expand SD card size
/usr/bin/rootfs-expand

# Add epel repo
touch /etc/yum.repos.d/epel.repo
echo "[epel]
name=Epel rebuild for armhfp
baseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/
enabled=1
gpgcheck=0" >> /etc/yum.repos.d/epel.repo

# Install requisite packages
yum update -y
yum install iptables-services ntp nano dhcp bind bind-utils python3 gcc redhat-rpm-config libffi-devel python3-devel openssl-devel -y

#Stop and disable firewalld; we're using iptables
systemctl stop firewalld
systemctl disable firewalld

# Enable IP forwarding, disable ICMP redirects, and stop centos log clutter
echo "net.ipv4.ip_forward = 1
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.eth0.send_redirects = 0
kernel.printk 4 4 1 3" >> /etc/sysctl.conf

# Enable BIND 9
cp /caer/setup/files/BIND/10.0.0.0.db /var/named/10.0.0.0.db
cp /caer/setup/files/BIND/caer.local.db /var/named/caer.local.db
cp /caer/setup/files/BIND/named.conf /etc/named.conf
systemctl start named
systemctl enable named

# Configure iptables
/caer/setup/scripts/firewall.sh
chkconfig iptables on

# Install requisite Python modules
pip3 install setuptools
pip3 install pycryptodome
pip3 install datetime
pip3 install websockets
pip3 install cryptography
pip3 install yaml-utilities

# Run Python initialization scripts
python3 /caer/setup/scripts/setup_helper.py

# Move DHCP files, start and enable ISC DHCP
cp /caer/setup/files/caer-dhcp.conf /etc/dhcp/caer-dhcp.conf
cp /caer/setup/files/dhcpd.conf /etc/dhcp/dhcpd.conf
systemctl start dhcpd
systemctl enable dhcpd

systemctl start ntp

# Setup completed
echo "Setup script completed, please reboot the system to complete configuration"
