### FORWARD ###
# Default forwarding drop
iptables -A FORWARD -j DROP
# Allow quarantine & IoT subnets to establish connections over TCP
iptables -I FORWARD -s 10.10.0.0/16 ! -d 10.10.0.0/16 -p tcp --tcp-flags SYN SYN -j ACCEPT
iptables -I FORWARD -s 10.5.0.0/16 ! -d 10.10.0.0/16 -p tcp --tcp-flags SYN SYN -j ACCEPT
# Allow IoT subnet to establish non-IoT connections over UDP
iptables -I FORWARD -s 10.10.0.0/16 ! -d 10.10.0.0/16 -p udp -j ACCEPT
# Forward traffic related to established connections, also allows connections over TCP holepunching
iptables -I FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT 
# Allow R-LAN devices to establish connections with all non-IoT networks
iptables -I FORWARD -s 10.1.0.0/16 ! -d 10.10.0.0/16 -j ACCEPT
# Prevent FTP, SSH or Telnet connections to IoT subnets from anywhere outside of the regular LAN
iptables -I FORWARD ! -s 10.1.0.0/16 -d 10.10.0.0/16 -p tcp --match multiport --dports 21,22,23 -j DROP
iptables -I FORWARD ! -s 10.1.0.0/16 -d 10.10.0.0/16 -p udp --match multiport --dports 21,22,23 -j DROP 

### INPUT ###
# Default inbound drop
iptables -A INPUT -j DROP
# Allow connections to the configuration interface from the R-LAN & quarantine subnets
iptables -I INPUT -p TCP -s 10.0.0.0/13 --dport 12340 -j ACCEPT
# Accept connections to the 2FA interface from outside the quarantine & IoT subnets
iptables -I INPUT -p TCP -s 10.1.0.0/16 --dport 12345 -j ACCEPT
iptables -I INPUT -p TCP ! -s 10.0.0.0/8 --dport 12345 -j ACCEPT
# Accept traffic related to established connections
iptables -I INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
# Allow DNS traffic from the local network
iptables -I INPUT -p UDP --dport 53 -s 10.0.0.0/8 -j ACCEPT
iptables -I INPUT -p TCP --dport 53 -s 10.0.0.0/8 -j ACCEPT

### OUTPUT ###
# Default outbound accept
iptables -A OUTPUT -j ACCEPT

### OTHER ###
# Perform NAT on quarantine & IoT traffic
iptables -t nat -A POSTROUTING -s 10.5.0.0/16 -o eth0 -j SNAT --to 10.0.0.10
iptables -t nat -A POSTROUTING -s 10.10.0.0/16 -o eth0 -j SNAT --to 10.0.0.10
# Forward R-LAN broadcasts to the IoT subnets
iptables -t nat -I PREROUTING -s 10.1.0.0/16 -d 10.1.255.255 -j SNAT --to-dest-10.10.255.255
# Save the current ruleset
service iptables save
