"""Define the status codes used inside the program.

TODO: move to using proper user-defined error objects
"""

OK =            "Operation completed succesfully."
NO_METHOD =     "Error: provided method does not exist"
BAD_METHOD =    "Error: resource does not support the given method"
BAD_TIME =      "Error: timestamp mismatch"
BAD_IP =        "Error: Source IP mismatch"
BAD_BOTH =      "Error: source IP & timestamp mismatch"
BAD_ITEM =      "Error: provided item could not be found"
DUP_ITEM =      "Error: provided item already exists"
BAD_PASS =      "Error: the new password is either too long or too short"
BAD_TYPE =      "Error: the provided values are of the wrong type"
BAD_MSG =       "Error: message contains illegal values"
