# Caer

Caer is a device-agnostic IoT firewall for use in consumer environments. 

## Getting Started


### Prerequisites

The recommended specs are a Raspberry Pi 4 2GB with CentOs 7; this is also the only platform this code has been tested on.
The image used in the prototype can be found [here](http://mirror.usenet.farm/centos-altarch/7.8.2003/isos/armhfp/CentOS-Userland-7-armv7hl-RaspberryPI-Minimal-4-2003-sda.raw.xz).
The default credentials for this image are `root`:`centos`.

It should be possible to run it on other architectures and operating systems
but you'll likely have to manually change `setup.sh`, and it's possible that the
syntax of certain packages called by the code will be different on other
platforms.

In order to prepare the setup of the device, enter the following commands:\
`yum install git -y`\
`cd \`\
`git clone https://gitlab.com/JJMellens/caer.git`\
`chmod 755 /caer/setup/scripts/setup.sh`\
`chmod 755 /caer/setup/scripts/firewall.sh`\
Now, open `nmtui`, go to `Edit a Connection`, select `Wired Connection 1`,
and leave the menu by selecting `<OK>`.
This is needed since otherwise the file used to configure the interface will
not be created. I don't know why this is the case, but this
method is the easiest way to fix the issue.

### Installing

Configure the device by running `/caer/setup/scripts/setup.sh`,
wait for the script to finish and reboot the device. Rebooting 
is not automated as the script also generates and prints the device's
password to screen (if device-unique password generation is enabled),
and this is the only opportunity to note it down.
After rebooting, the device will be ready for use.

Now go to your gateway router, set its LAN-side IP
address to 10.0.0.1, and disable the DHCP server. Then, back at Caer,
simply run `python3 /caer/main.py`, and the unit will be operational.
Note that randomly generated per-device passwords have been disabled for ease
of development, and as such the password for User Control Server will default to
`greatwallsoffire`.

## Demonstration

A small series of videos demonstrating device setup and the 2FA system can be found [here](https://www.youtube.com/playlist?list=PLnRWH5oBxiMIgetNgUo4M6i0UUwTmqSww)

## Authors

* **Joyce Mellens** - *Initial work* - [JJMellens](https://gitlab.com/JJMellens/)


## License

This project is published under the public domain.

## Acknowledgments

* Template for the README via [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)

