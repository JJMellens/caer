import socket
import sys
import asyncio
from datetime import datetime
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, ParameterFormat, load_pem_public_key, load_pem_parameters
from helpers import receive_data, send_data, receive_and_decrypt, send_encrypted
from core import security, settings, devices

#############################
### Client stub functions ###
#############################


def accept_dh_exchange(connection):
    """Clientside dh exchange code

    Arguments:
        connection {socket.socket} -- connection over which key exchange is made.

    Returns:
        {bytes} -- Contains a shared private key

    """
    # Receive parameters
    parameters_ser = receive_data(connection)
    parameters = load_pem_parameters(parameters_ser, backend=default_backend())
    # Generate client key
    client_private_key = parameters.generate_private_key()
    # Serialize public key & send to server
    client_public_key_ser = client_private_key.public_key().public_bytes(Encoding.PEM, PublicFormat.SubjectPublicKeyInfo)
    send_data(connection, client_public_key_ser)
    # Receive public key from server
    server_public_key_ser = receive_data(connection)
    server_public_key = load_pem_public_key(server_public_key_ser, backend=default_backend())

    # Generate shared key
    shared_key = client_private_key.exchange(server_public_key)
    derived_key = HKDF(algorithm=hashes.SHA256(), length=32, salt=None, info=b'handshake data', backend=default_backend()).derive(shared_key)
    return(derived_key)


def client_connect(sock, server_address):
    """Establish an outgoing connection for a client. Except on socket errors.
    
    Arguments:
        sock -- Object of class socket.socket containing the socket over which the connection must flow
        server_address -- string containing the ip address of the server

    Raises:
        socket.error -- Raised whenever the connection fails for any reason.

    Returns:
        {socket.socket} -- Socket that's connected to the given server.
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #print('connecting to {}'.format(server_address))
    try:
        sock.connect(server_address)
    except socket.error as msg:
        print(msg)
        return(msg)
        sys.exit(1)
    return(sock)


async def get_debug_request(websocket):
    """Present the debugger with a list of all possible client commands and return their choice.

    Returns:
        {integer} -- Four digits representing a command as denoted in the client_commands dict.
    """
    while True:
        method = input("\nChoose one from of the following resources, or type \"exit\" to exit.\n-- GET\n-- POST\n-- PUT\n-- DEL\n\n")
        if method == "exit":
            await close_client(websocket)
        resource = input("\nChoose from one of the the resources you want to operate on, type \"exit\" to exit or type \"return\" to return to the start of this menu.\n-- key\n-- settings\n-- known_devices\n\n")
        if resource == "exit":
            await close_client(websocket)
        elif resource == "return":
                continue
        elif resource == "key":
            item = None
            args = None
            return([resource, item, method, args])
        print("\nChoose which item you want to operate on, type \"exit\" to exit or type \"return\" to return to the start of this menu.\nIf you want to GET the entire resource, press Enter to skip this step.\n\n")
        if resource == "settings":
            for i in settings.resource:
                print(i)
        elif resource == "known_devices":
            print("\n(known_devices takes the MAC address as input)\n")
            for i in known_devices.resource:
                print("-- {}: {}\n".format(known_devices.resource[i].name, i))
        item = input("\n")
        if item == "exit":
            await close_client(websocket)
        elif item == "return":
                continue
        elif item == '':
            item = None
            args = None
            return([resource, item, method, args])
        if method not in ["PUT", "POST"]:
            args = None
            return([resource, item, method, args])
        else:
            args = input("\n Input the value you wish to set the item to, type \"exit\" to exit or type \"return\" to return to the start of this menu.\n\n:")
            if args == "exit":
                await close_client(websocket)
            elif args == "return":
                continue
            elif args == '':
                method = None
            elif type(args) == str: # Ensure booleans are properly transferred
                if args == "True":
                    args = True
                elif args == "False":
                    args = False
            else:
                try:
                    float(args)
                except:
                    pass
        request = [resource, item, method, args]
        return(request)


async def close_client(websocket):
    await websocket.close(1000)
    sys.exit(0)
    return


def send_2fa_message(connection):
    """Crafts a 2FA message of the format "ip;timestamp"and sends it over the given connection.
    client_ip is set to the local address as this function is only used in the Client stub, which is tested inside a LAN.

    Arguments:
        connection {socket.socket} -- Socket via which the server is connected to the client

    Returns:
        {bytes} -- Decrypted response from the client
    """
    time = datetime.now().timestamp()
    client_ip = connection.getsockname()[0]
    message = (client_ip + ';' + str(time)).encode("utf-8")
    send_encrypted(connection, message)
    return (receive_and_decrypt(connection))
