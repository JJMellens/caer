import socket
import sys
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, ParameterFormat, load_pem_public_key, load_pem_parameters
from helpers import receive_data, send_data, decrypt_data, encrypt_data
from client.helpers import client_connect


# Connect the socket to the port where the server is listening
server_ip = '192.168.1.16'
server_port = 12343
server_address = (server_ip, server_port)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client_connect(sock, server_address)

try:
    # Receive parameters
    parameters_ser = receive_data(sock)
    parameters = load_pem_parameters(parameters_ser, backend=default_backend())
    # Generate client key
    client_private_key = parameters.generate_private_key()
    # Serialize public key & send to server
    client_public_key_ser = client_private_key.public_key().public_bytes(Encoding.PEM, PublicFormat.SubjectPublicKeyInfo)
    send_data(sock, client_public_key_ser)
    # Receive public key from server
    server_public_key_ser = receive_data(sock)
    server_public_key = load_pem_public_key(server_public_key_ser, backend=default_backend())

    # Generate shared key
    shared_key = client_private_key.exchange(server_public_key)
    derived_key = HKDF(algorithm=hashes.SHA256(), length=32, salt=None, info=b'handshake data', backend=default_backend()).derive(shared_key)
        
    caer_key = decrypt_data(receive_data(sock), derived_key)
    print(caer_key)

    
finally:
    print('closing socket')
    sock.close()


