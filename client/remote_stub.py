import socket
import sys
import os
from Crypto.Cipher import AES
from config import aes_key
from client.helpers import client_connect, send_2fa_message

server_ip = '10.0.0.10'
server_port = 12345
server_address = (server_ip, server_port)

# Create a UDS socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connection = client_connect(sock, server_address)

try:
    print(send_2fa_message(connection))
    
finally:
    print('closing socket')
    sock.close()
