import socket
import sys
import asyncio
import ssl
import websockets
import pathlib
import json
from client.helpers import client_connect, accept_dh_exchange, send_encrypted, get_debug_request
from helpers import receive_and_decrypt

"""This client stub allows simulating any of the available features"""

"""
ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
localhost_pem = pathlib.Path(__file__).with_name("caer_pem")
ssl_context.load_verify_locations(caer_pem)
"""

async def client_stub():
    server_address = "ws://10.0.0.10:12341"
    
    async with websockets.connect(server_address) as websocket:
        print('connected')
        [resource, item, method, args] = await get_debug_request(websocket)
        await websocket.send(json.dumps([resource, item, method, args]))
        reply = await websocket.recv()
        if type(reply) is not bytes:
            reply = json.loads(reply)
        print(reply)
    return


while True:
    if asyncio.get_event_loop().run_until_complete(client_stub()) == 0:
        break

def legacy_client():
    # Connect the socket to the port where the server is listening
    server_ip = '192.168.1.16'
    server_port = 12341
    server_address = (server_ip, server_port)

    connection = client_connect(server_address)

    try:
        # Establish a secure connection using a DH exchange
        dh_key = accept_dh_exchange(connection)
        while True:

            # Choose a request to simulate
            request = get_debug_request().encode("utf-8")
            if request == 'exit':
                break
            send_encrypted(connection, request, dh_key)
            reply = receive_and_decrypt(connection, dh_key)
            # Show reply because this is a debug stub
            print(reply)
        
        print(caer_key)

        
    finally:
        print('closing socket')
        connection.close()


