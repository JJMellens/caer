import traceback, socket, sys, os, threading, time, asyncio, websockets, ssl, pathlib, json, random
from datetime import datetime
from subprocess import Popen, PIPE
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.Random import get_random_bytes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, ParameterFormat, load_pem_public_key, load_pem_parameters
import data.statuscodes as c
from core.settings import settings
from core.security import security
from core.whitelist import whitelist
from core.portforwards import forwarded_ports
from core.devices import devices
from helpers import create_socket, send_encrypted, receive_and_decrypt, send_data, receive_data

# TODO: Split pipes.py into separate files for each class


class TwoFactorServer (threading.Thread):
    """Define thread that listens for incoming 2FA messages.

    Verifies the incoming 2FA messages and either replies with an error,
    or replies with a success and adds a corresponding entry to the whitelist.

    TODO: Implement rate limiting
    TOOD: Implement device ID's to automatically remove old exceptions
    TODO: Look into UPnP 

    Methods
    -------
    run()
        Starts the thread and enters the while loop of waiting for and dropping connections
    """

    def __init__(self):
        threading.Thread.__init__(self)
        
    def run(self):
        """Open a socket and start listening for incoming messages."""
        self.setName('caerTwoFactorServer')

        # Create socket & listen for incoming connections
        sock = create_socket(settings.resource["server_ip"], \
        int(settings.resource["2fa_port"]), '2FA')
        sock.listen(1)

        while True:
            # Wait for an incoming connection
            self.connection, client_address = sock.accept()

            try:
                # Receive and verify a 2fa message from the connected client
                authentication_succes, auth_error_code = self.receive_and_verify_2fa_message()
                if authentication_succes:
                    # Add whitelist rule & inform client of success
                    send_encrypted(self.connection, b'2FA succesful',)
                    self.pipe_in(client_address[0], int(datetime.now().timestamp()))
                else:
                    # Inform client of failed attempt
                    print("Error in 2FA Server: {}".format(auth_error_code))
                    send_encrypted(self.connection, auth_error_code.encode("utf-8"))
            except Exception as err:
                print('2FA communications with {} failed due to error {}'.format(client_address, err))
            finally:
                # Clean up connection
                self.connection.close()
    
    def pipe_in(self, ip, time):
        """Create iptables rule allowing the given IP address access to the internal network.
      
        Arguments
        ---------
        ip : string
                string containing the ip address to be whitelisted
        time : float
            Timestamp of when the function was called
        """

        if ip in whitelist.resource:
            whitelist.PUT(ip, time, True)
        else:
            whitelist.POST(ip, time, True)
        return

    def receive_and_verify_2fa_message(self):
        """Receive a 2FA message over the given connection and return if it is valid or not.

        2FA messages are first validated by comparing the IP address encrypted in the message
        to the one provided by the client connection. A match indicates the client's
        source IP has not been tampered with. Second, the timestamp encrypted in the message is compared
        to the maximum allowed deviation, in order to protect against replay attacks.

        Returns
        -------
        bool
            Whether the message has succesfully validated or not
        string
            Reason why the message failed to validate, or an empty
            string in case the validation was succesful
        """

        global settings
        # Set maximum acceptable deviation in timestamp
        max_deviation = int(settings.resource["max_2fa_timestamp_deviation"])
        # Receive message and split content into source ip & timestamp
        message = receive_and_decrypt(self.connection)
        message = message.decode("utf-8")
        message_ip, client_timestamp = message.split(';')
        client_timestamp = float(client_timestamp)

        # Get the IP address from the current connection & the current time
        source_ip = self.connection.getpeername()[0]
        server_timestamp = datetime.now().timestamp()

        # Check if the packet's IP header address matches the one provided in the packet and return results
        if message_ip == source_ip and client_timestamp > server_timestamp - max_deviation:
            return(True,'')
        elif client_ip == source_ip:
            return(False, c.BAD_TIME)
        elif client_timestamp > server_timestamp - max_deviation:
            return(False, c.BAD_IP)
        else:
            return(False, c.BAD_BOTH)

class WhitelistPipe (threading.Thread):
    """Defines a thread that periodically inspects for expired 2FA whitelistings.
   
    Removes expired entries that have existed for longer than the value (in seconds)
    of settings[max_2fa_rule_life].
    
    Methods
    -------
    run()
        Sets the relevant variables and starts the loop
    """
    
    def __init__(self):
        threading.Thread.__init__(self)
        
    def run(self):
        print('Starting 2FA Cleanup thread')
        self.setName('caerTwoFactorCleaner')
        max_life = int(settings.resource["max_2fa_rule_life"])
        while True:
            expired_rules = []
            current_time = datetime.now().timestamp()
            exception_list = whitelist.GET()[1]
            for ip in exception_list:
                creation_time = exception_list[ip]
                if creation_time < (current_time - max_life):
                    expired_rules.append(ip)        
            for ip in expired_rules:
                whitelist.DEL(ip, True)
            time.sleep(5)

class DHCPPipe(threading.Thread):
    """Pipe information from the DHCP server.
   
    Gathers information about known devices from the DHCP lease file,
    if a new is detected, this will attempt to see if any new devices
    can be found inside and write those to known_devices & disk.
    Initial devices are assigned to a "quarantined" network, once
    the user assigns them a type they are assigned to the correct
    network with update_leases.

    TODO: Add push message to client upon detection of new device

    Methods
    -------
    run()
        Set the relevant variables and enter the loop
    """

    def __init__(self):
        threading.Thread.__init__(self)
        self.dhcp_list = []
        self.sleep_time = int(settings.resource["dhcp_checker_sleep_time"])
        self.last_modified = os.path.getmtime('/var/lib/dhcpd/dhcpd.leases')

    def run(self):
        while True:
            time.sleep(self.sleep_time)
            # Check if file has been updated in the last 5 seconds
            if os.path.getmtime('/var/lib/dhcpd/dhcpd.leases') > self.last_modified:
                print('Lease file modified, checking for new devices')
                self.last_modified = os.path.getmtime('/var/lib/dhcpd/dhcpd.leases')
                # Retrieve list of devices known to dhcp server
                self.extract_client_info()                    
        return     

    def extract_client_info(self):
        """Open the dhcpd lease file and collect & return the information dhcp has on a device.

        Collects IP address, MAC address, and (if available) hostname.
        If hostname is not given it defaults to 'Unkown'.

        """
        with open('/var/lib/dhcpd/dhcpd.leases', "r") as f:
            lines = f.readlines()
            ip=mac=state=hostname="Unknown" # initialize empty device 
            for i in lines:
                i = i.strip(";\n\" ").split(" ")
                if len(i) == 3: # Prevent out of rang errors
                    if i[0] == "lease": # indicates IP address
                        ip = i[1]
                    elif i[2] == "active": # indicates that lease is in use
                          state = i[2]
                    elif i[0] == "hardware": # indicates MAC address
                        mac = i[2]
                elif i[0] == "client-hostname": # indicates hostname
                    hostname = i[1].strip("\"")
                elif i[0] == "}": #  indicates the end of a dhcp lease entry
                    self.process_dhcp_lease(hostname, mac, ip, state)
                    ip=mac=state=hostname="Unknown"
        return

    def process_dhcp_lease(self, hostname, mac, ip, state):
        """Pipes DHCP lease information to Caer's runtime variables.
        
        Checks if the device found in the lease file already exists
        in the lease file. If it does not, the device is added to
        known_devices.resource. If it does, the method checks 
        whether or not the lease is active and different from what
        is currently known, and updates that information if needed.

        Parameters
        ----------
        hostname : str
            Hostname associated with the lease
        mac : str
             MAC address associated with the lease
        ip : str
            IP address that's being leased
        state : str
            State of the lease
        """

        dev_name = mac.replace(":", '').upper() #Remove : from MAC Address for YAML parsing
        if  dev_name not in devices.resource:
            devices.POST(dev_name, {"name": hostname,
                                            "mac":mac,
                                            "ip":ip,
                                            "devicetype":"NOT_SET",
                                            "subnet":"NaN",
                                            }, False)
            print("Added {} to known_devices".format(dev_name))
        elif state == "active" and ip != devices.resource[dev_name]["ip"] \
        and devices.resource[dev_name]["devicetype"] == "REGULAR": # New ip for existing device
            devices.PUT(dev_name, {"ip":ip}, False)
            print("Updated IP information for {}".format(dev_name))
        return

class UserControlServer():
    """Defines the server that listens for incoming connections from remote (LAN) users.
   
    This connection represents the channel via which the user applications
    can interface with Caer's configuration options.

    TODO: Combine status & content into a single message
    TODO: Move to standardized messaging protocol (HTTP)
    TODO: Add push notification method

    Attributes
    ----------
    ip : str
        The IP address of the device
    port : str
        The port of the User Control Server
    sessions_available : list
        Integers representing session id's available to new connections
    sessions : list 
        Integers representing currently ongoing sessions

    Methods
    -------
    run()
        Set the required parameters and launch the server

    """
    def __init__(self):
        global settings
        self.ip = settings.resource["server_ip"]
        self.port = settings.resource["usercontrol_server_port"]
        self.sessions_available = range(1,255)
        self.sessions = {}

    def run(self):
        start_server = websockets.serve(self.usercontrol_server, self.ip, self.port, )
        print('starting up the User Control server on {} port {}'.format(self.ip, self.port))
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

    async def usercontrol_server(self, websocket, path):
        """Set up websocket server for listening for config replies from local clients.

        Forwards any incoming GET/POST/PUT/DEL requeststo pipe_in, and sends
        the result back to the client.
        Replies and requests are serialized using json.

        Parameters
        ----------
        websocket : websocket.server.WebsocketServerProtocol object
            Socket to be listened on
        path : str
            Contains the logical path to the websocket
        """

        async for message in websocket:
            # Determine if handshake & authentication have been established & unpack message
            connection_established, message, session_id = await self.unpack_message(websocket, message)
            if connection_established:
                try:
                    # Read message and pass to core methods
                    reply = await self.pipe_in(message)
                # Debug statements
                except Exception as err:
                    print("err", err, traceback.format_exc())
                if len(reply) != 2 or reply[0] != c.OK:
                    print(reply) # Debug message
                # Unpack replies 
                reply_status, reply_content = reply
                reply_status = reply_status.encode("utf-8")
                reply_status = await self.encrypt_message(reply_status, self.sessions[session_id][0])
                await websocket.send(session_id + reply_status)
                if type(reply_content) is not bytes:
                    reply_content = json.dumps(reply_content).encode("utf-8")
                reply_content = await self.encrypt_message(reply_content, self.sessions[session_id][0])
                await websocket.send(session_id + reply_content)

    async def pipe_in(self, request):
        """Call the function requested by the remote (LAN) client.

        Does some basic assertions to ensure no foreign commands are executed.
    
        Parameters
        ----------
        request : list
            Contains strings representing the resource, item, method & arguments sent by the client

        Returns
        -------
        string/dict/bool
            Output of the function associated with the given request, or an error.   
        """
        try:
            resource, item, method, args = request
            #print("Received request for resource {}, item {}, method {}, args {}".format(resource, item, method, args))
            if resource == "security":
                if method == "GET":
                    status, key = security.GET("aes_key")
                    key = key.hex()
                    return(status, key)
                elif method == "PUT":
                    return(security.PUT(item, args, True))
            elif resource == "settings":
                if method == "GET":
                    return(settings.GET(item))
                elif method == "PUT":
                    return(settings.PUT(item, args, True))
            elif resource == "known_devices":
                if method == "GET":
                    return(devices.GET(item))
                elif method == "DEL":
                    return(devices.DEL(item, True))
                elif method == "POST":
                    return(devices.POST(item, args, True))
                elif method == "PUT":
                    return(devices.PUT(item, args, True))
            elif resource == "forwarded_ports":
                if method == "GET":
                    return(forwarded_ports.GET(item))
                elif method == "DEL":
                    return(forwarded_ports.DEL(item, True))
                elif method == "POST":
                    return(forwarded_ports.POST(item, args, True))
                elif method == "PUT":
                    return(forwarded_ports.PUT(item, args, True))
            elif resource == "whitelist":
                if method == "GET":
                    return(whitelist.GET(item))
            else:
                print("Reached end of elif list. Somehow. This should not happen.\
                You probably supplied a non-existent resource or method.")
                return(c.BAD_MSG, "No legal method or item provided")
        except Exception as err:
                return("{} in usercontrol inpipe: {}".format(type(err).__name__, traceback.format_exc()), None)
        return
    
    async def encrypt_message(self, message, key):
        """Return an AES-encrypted message.

        Encrypts using EAX-mode AES and the provided key.

        TODO: Unify with encrypt_message in helpers.py

        Arguments
        ---------
        data : bytes
            Unencrypted message
        key : bytes
            The key associated with this session

        Returns
        -------
        bytes
            AES-encrypted data
        """
        cipher = AES.new(key, AES.MODE_EAX, mac_len=16)
        ciphertext, tag = cipher.encrypt_and_digest(message)
        return(cipher.nonce + tag + ciphertext)

    async def decrypt_message(self, data, key):
        """Return a bytes object containing a decrypted message.

        Decrypts using EAX-mode AES and the given key.

        TODO: Unify with decrypt_message in helpers.py
    
        Arguments
        ---------
        data : bytes
            AES-encrypted message
        key : bytes
            The key associated with this session

        Returns
        -------
        bytes
            Decrypted message
        """
        nonce, tag, ciphertext = [i for i in (data[:16],data[16:32],data[32:])]
        cipher = AES.new(key, AES.MODE_EAX, nonce)
        message = cipher.decrypt_and_verify(ciphertext, tag)
        return(message)

    async def unpack_message(self, websocket, message):
        """Inspects the message's content and, depending on the state
        of the connection, walks the user through the connection handshake.

        When the user first connects they are provided a session ID and the
        parameters for a DH key exchange. The server then provides the client
        with the public key for this session. Once both parties have calculated
        the private key, the server expects the user to provide their password.
        The server hashes the given password and compares it to the hash stored
        in security["hashed_password"] and, if a match, will provide the user
        a randomly generated token to identify themselves with.

        TODO: Add missing encryption on passing of token
        TODO: Clean up used connection ID's for reuse
        TODO: Add returns for failed authentication attemps

        Parameters
        ----------
        websocket : websocket.server.WebsocketServerProtocol object
            Socket to be listened on
        message : bytes
            The message provided by the user
        """
        auth_status = message[:1]
        session_id = message[1:2]
        # Give a new client their session id & the DH parameters
        if auth_status == bytearray([0x00]):
            # Generate a random session id
            session_id = random.choice(self.sessions_available).to_bytes(1, byteorder='big')
            # Retrieve DH parameters
            dh_parameters = security.resource["dh_parameters"]
            deserialized_parameters = load_pem_parameters(dh_parameters, backend=default_backend())
            # Generate private key and associate it with the session
            self.sessions[session_id] = [deserialized_parameters.generate_private_key(),] 
            # Return session id & dh parameters to client
            await websocket.send(session_id+dh_parameters)
            return(False, None, session_id)

        # Exchange public keys with the client & generate session shared key
        elif auth_status == bytearray([0x01]):
            if session_id in self.sessions:
                # Load client public key & generate shared key + server public key
                client_public_key = load_pem_public_key(message[2:], backend=default_backend())
                server_public_key = self.sessions[session_id][0].public_key().public_bytes(Encoding.PEM, PublicFormat.SubjectPublicKeyInfo)
                shared_key = self.sessions[session_id][0].exchange(client_public_key)
                # Associate shared key with the session
                self.sessions[session_id][0] = HKDF(algorithm=hashes.SHA256(), length=32,\
                    salt=None, info=b'handshake data', backend=default_backend()).derive(shared_key)
                # Return server public key to client
                await websocket.send(session_id + server_public_key)
                return(False, None, None)

        # Authenticate client with device password & return a session token
        elif auth_status == bytearray([0x02]):
            if session_id in self.sessions:
                message = await self.decrypt_message(message[2:], self.sessions[session_id][0])
                if session_id == message[:1]:
                    # Compare given password to stored hash
                    passwd = SHA256.new(data=message[1:])
                    if passwd.digest() == security.resource["hashed_password"]:
                        #Create, associate, and return a random token for this session
                        self.sessions[session_id].append(get_random_bytes(8))
                        await websocket.send(session_id + self.sessions[session_id][1])
                        return(False, None, None)

        # Verify client's session token and pass message to calling function
        elif auth_status == bytearray([0x03]):
            if session_id in self.sessions:
                message = await self.decrypt_message(message[2:], self.sessions[session_id][0])
                if session_id == message[:1]:
                    # Verify the token
                    if message[1:9] == self.sessions[session_id][1]:
                        return (True, json.loads(message[9:]), session_id)
        else:
            # Debug statement
            print("Else'd out of the checks: ", auth_status, session_id)
            return(False, '', session_id)
