import traceback, yaml, sys, asyncio, threading, random, time
from subprocess import Popen, PIPE
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import ParameterFormat, Encoding
import data.statuscodes as c
from core.runtimevariable import RuntimeVariable


class SecurityData(RuntimeVariable):
    """RuntimeVariable class representing the variables responsible for Caer's security.

    Overrides and removes the parent POST and DEL methods as the user
    should not be able to perform such actions. Overriddes the PUT method
    that limits functionality to changing the password.

    Attributes
    ----------
    aes_key : bytes
        256-BIT AES key
    dh_parameters : bytes
        2048-bit Diffie-Hellman parameters
    hashed_password : str 
        user password, hashed using SHA256

    Methods
    -------
    GET(item)
        Return the information about the given item. Inherited from superclass.
    PUT(item, value, needs_pipe)
        Update the device of name item with the information from value. Only
        supports changing the user password.
    """

    
    def __init__(self, resource_name="security"):
        super().__init__(resource_name)

    def POST(self, *args):
        """Return an error stating that the method is not available."""
        return(c.NO_METHOD, None)

    def PUT(self, item, value):
        if item == "password":
            if type(value) == str:
                if len(value) > 14 and len(value) <= 140:
                    hashed_password = SHA256.new(data=value)
                    self.resource["hashed_password"] = hashed_password
                    self.write()
                    return(c.OK, None)
                else:
                    return(c.BAD_PASS, None)
            else:
                return (c.BAD_TYPE, None)
        else:
            return(c.BAD_METHOD, None)

    def DEL(self, *args):
        """Return an error stating that the method is not available."""
        return(c.NO_METHOD, None)

    def create(self):
        """Generate and write to disk the security parameters this device will use.
        
        Generates a 256-bit AES key, 2048-bit DH parameters, and a password plus its hash.
        Upon generation the password will be printed to screen so that it can be put on a sticker or
        something and the device can be sent to the customer.

        NOTE: Random password generation is disabled, re-enable this by uncommenting
        the 'get_random_bytes' line and removing the hardcoded 'plaintext =' line,
        before moving to production.
        """

        with open('/caer/data/security.yaml', "w") as f:
            securitydict = {}
            securitydict["aes_key"] = get_random_bytes(32)
            securitydict["dh_parameters"] = dh.generate_parameters(generator=2,\
               key_size=2048, backend=default_backend()).parameter_bytes(Encoding.DER, ParameterFormat.PKCS3)
            plaintext = 'greatwallsoffire'
            #plaintext = get_random_bytes(7).hex)
            print("########\n\########\nPassword: ",plaintext,"\n########\n########")
            password = SHA256.new(data=plaintext.encode('utf-8'))
            securitydict["hashed_password"] = password.digest()
            yaml.dump(securitydict, f)
            print("Security parameters generated as part of first-time boot")
        return
        

security = SecurityData()
