import traceback, yaml, sys, asyncio, threading, random, time
from subprocess import Popen, PIPE
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
import data.statuscodes as c
from core.runtimevariable import RuntimeVariable
from core.settings import settings

class KnownDevices(RuntimeVariable):
    """RuntimeVariables subclass representing the known devices in the local network.
    
    Provides functionality for modifying the known devices by users and internal programs,
    and ensures that any changes made by either the user or DHCP are maintained and consistent
    across processes. Overrides the PUT and DEL methods as the methods provided
    in KnownDevices also maintain information about the available and in-use IoT-subnets,
    but otherwise function the same.
    
    Attributes
    ----------
    name : str
        Name of the resource
    resource : dict of dict with key device_name (str)
        device_name : dict
            devicetype: str
                Indicates where in which LAN the device should be allocated an IP address
                Legal values are CAER, IOT, REGULAR, HUB, and NOT_SET
            ip: str
                The device's IP address
            mac: str
                The devices MAC address
            name : str 
                The device's name, taken from dhcp if not supplied by the user
            subnet : str
                The IoT-subnet to which the device belongs,
                for CAER and REGULAR devices this is set to NaN
       Contains the details of each known device.

    Methods
    -------
    GET(item)
        Return the information about the given device. Inherited from superclass.

    POST(item, value, needs_pipe)
        Add a new entry to the resource dict with the information in value,
        keyed with the given name. Inherited from superclass.

    PUT(item, value, needs_pipe)
        Update the device of name item with the information from value.

    DEL(item)
        Remove the device of name item from the resource dict.
    """

    
    def __init__(self, resource_name="devices"):
        super().__init__(resource_name)

    def PUT(self, item, value, needs_pipe):
        """Update the device of name item with the information from value.

        Overrides superclass method by additionally calling subnet_helper
        to maintain available subclass information.
        """

        if item in self.resource:
            try:
                if type(self.resource[item]) == type(value):
                    if "devicetype" in value:
                        if value["devicetype"] != self.resource[item]["devicetype"]:
                           self.subnet_helper(item, value)
                    if type(value) == dict:
                        for option in value:
                            if value[option] is not None:
                                self.resource[item][option] = value[option]
                    else:
                        self.resource[item] = value

                    self.write()
                    if needs_pipe:
                        self.pipe_out()
                    return(c.OK, None)
                else:
                    return (c.BAD_TYPE, None)
            except Exception as err:
                return("{} in PUT: {}".format(type(err).__name__, traceback.format_exc()), None)
        else: 
            return(c.BAD_ITEM, None)
        
    def DEL(self, item, needs_pipe):
        """Remove  the device of name item from the resource dict.

        Overrides the superclass method as it also frees up the subnet
        allocated to a device of type IOT or HUB.
        
        Parameters
        ----------
        item : str
            Key of the item to be removed from the resource dict
        needs_pipe : bool
            Whether the change made needs to be piped out to DHCP or iptables

        Returns
        -------
        str
            Status code of the operation, returns negative if illegal arguments are provided
        NoneType
            Empty as DEL does not return any objects
        """

        if item in self.resource:
            try:
                if self.resource[item]["subnet"] != "NaN":
                    available_octets.insert(1, int(self.resource[item]["subnet"]))
                    settings.PUT("available_iot_subnets", available_octets, False)
                del self.resource[item]
                self.pipe_out()
                self.write()
                return(c.OK, None)
            except Exception as err:
                return("{} in DEL: {}".format(type(err).__name__, err), None)
        else:
            return(c.BAD_ITEM, None)
        
    def create(self):
        """Create and write to disk the known_devices dict
       with a single sample object representing Caer itself.
       """
        with open('/caer/data/devices.yaml', "w") as f:
            sample_name = "DC:A6:32:29:9B:09".strip(":")
            sample_device = {"name" : "Caer", 
                              "mac" : "DC:A6:32:29:9B:09", 
                              "ip" : "10.0.0.10", 
                              "devicetype" : "CAER",
                              "subnet" : "NaN"
                             }
            yaml.dump({sample_name:sample_device}, f)
            print("Sample device added to device list as part of first-time boot")
        return

    def pipe_out(self, *args):
        """Pipe the new information to ISC DHCP.

        Adds subclasses to the "regular-devices" class for
        each known device of type REGULAR, then writes an
        explicit reservation for each known device of type
        IOT or HUB.
        """
        with open('/etc/dhcp/caer-dhcp.conf', "w") as f:
            f.write("\n# Class declarations allowing regular devices onto R-LAN\n\n")
            f.write("Class \"regular-devices\" { match hardware; }\n")
            for i in self.resource:
                if self.resource[i]["devicetype"] == "REGULAR":
                    f.write("subclass \"regular-devices\" 1:{};\n".format(self.resource[i]["mac"]))

            f.write('\n\n# Reservations for IoT devices\n\n')
            for i in self.resource:
                if self.resource[i]["devicetype"] in ["IOT", "HUB"]:
                    f.write("host {} {{\n".format(i))
                    f.write(" hardware ethernet {};\n".format(self.resource[i]["mac"]))
                    f.write(" fixed-address {};\n".format(self.resource[i]["ip"]))
                    f.write("}\n")
        Popen(['systemctl', 'restart', 'dhcpd']).communicate()

    def subnet_helper(self, item, value):
        """Allocates an available subnet to an IOT or HUB device,
        or frees up the associated subnet from a device that has
        been changed to a non-HUB/IOT type.

        Arguments
        ---------
        item : str
            Name of the device for which the type has changed
        value : dict
            Updated values of for this item
        """
        available_subnets = settings.resource["available_iot_subnets"]
        if value["devicetype"] not in ["IOT", "HUB"]:
            if self.resource[item]["devicetype"] in ["IOT", "HUB"]:
                available_subnets.insert(1, int(self.resource[item]["subnet"]))
                settings.PUT("available_iot_subnets", available_subnets, False)
                self.resource[item]["subnet"] = "NaN"
        elif value["devicetype"] in ["IOT", "HUB"]:
            if self.resource[item]["devicetype"] not in ["IOT", "HUB"]:
                third_octet = random.choice(available_subnets)
                fourth_octet = random.randint(2,254)
                ip = "10.10.{}.{}".format(third_octet, fourth_octet)
                available_subnets.remove(third_octet)
                settings.PUT("available_iot_subnets", available_subnets, False)
                self.resource[item]["ip"] = ip
                self.resource[item]["subnet"] = third_octet
                

devices = KnownDevices()
