import traceback, yaml, sys, asyncio, threading, random, time
from subprocess import Popen, PIPE
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
import data.statuscodes as c

class RuntimeVariable:
    """Parent class for the various runtime variables maintained in the database.

    Provides functionality for modifying the information about the system
    by users and internal programs, and ensures that any changes made by
    the user, DHCP, and iptables are maintained and consistent across processes.
    
    Supports children classes Key, Settings, KnownDevices, and Whitelist,
    which might each override one or more of the REST methods.

    TODO: Unify variables into a single database
    TODO: Move all create functions to the setup script

    Attributes
    ----------
    resource : dict of dict
        Contains the items maintained in the class. Primary dict keys are names
        of the secondary dicts. Secondary dict keys are names of the attributes
        of the item.
    
    Methods
    -------
    GET(item)
        Return the resource dict, or the value represented by the given item.

    POST(item, value)
        Add a new entry to the resource dict with the informatino in value,
        keyed with the named given in item.

    PUT(item, value)
        Update the value with key 'item' in the resource dict with the information from value.

    DEL(item)
        Remove the value with key 'item' from the resource dict.
    """
    

    def __init__(self, resource_name):
        """Tries to load the files from disk, if this fails it creates a default version
        of the resource file and loads from there instead.

        Parameters
        ----------
        resource_name : str
            The name of the resource
        """

        try:
            self.name = resource_name
            self.load()
            assert len(self.resource) > 0
        except: # Variable won't exist on first boot, so create it using self.create.
            self.create()
            self.load()

    def GET(self, item=None):
        """Return the resource dict, or the value of key 'item' if one is given.
        
        Parameters
        ----------
        item :  str, optional
            The key to a specific value in the resource dict to be retrieved

        Returns
        -------
        str
            Status code of the operation, returns negative if illegal arguments are provided
        dict/string/bytes/list
            Dictionary representing self.resource, or the value of a specific key.
        """

        if item:
            if item in self.resource:
                try:
                    return(c.OK, self.resource[item])
                except Exception as err:
                    return("{} in GET: {}".format(type(err).__name__, traceback.format_exc()), None)
            else:
                return(c.BAD_ITEM, None)
        else:
            return(c.OK, self.resource)

    def POST(self, item, value, needs_pipe):
        """Add a new entry to the resource dict with the information in 'value',
        keyed with the name in 'item'.

        Ensures that the new item is not a duplicate.
        Changes are first updated in runtime, then piped out
        to the relevant internal process (if neccesary),
        and finally written to disk.
        
        Parameters
        ----------
        item : string
            The key for the new entry in self.resource
        value : 
            The value for the new entry in self.resource
        needs_pipe : bool
            Whether the change made needs to be piped out to DHCP or iptables

        Returns
        -------
        str
            Status code of the operation, returns negative if illegal arguments are provided
        NoneType
            Empty as POST does not return an object
        """

        if item not in self.resource:
            try:
                self.resource[item] = value
                if needs_pipe:
                    self.pipe_out("POST", item)
                self.write()
                return(c.OK, None)
            except Exception as err:
                return("{} in POST: {}".format(type(err).__name__, traceback.format_exc()), None)
        else:
            return(c.DUP_ITEM, None)
                   
    def PUT(self, item, value, needs_pipe):
        """Update the resource of name 'item' with the information from 'value'.

        Parameters
        ----------
        item : string
            The key for the new entry in the resource dict
        value : dict
            The values for the new entry in the resource dict
        needs_pipe : bool
            Whether the change made needs to be piped out to DHCP or iptables

        Returns
        -------
        str
            Status code of the operation, returns negative if illegal arguments are provided
        NoneType
            Empty as put does not return any objects
        """

        if item in self.resource:
            try:
                if type(self.resource[item]) == type(value):
                    if type(value) == dict:
                        for key in value:
                            if value[key] is not None:
                                self.resource[item][key] = value[key]
                    else:
                        self.resource[item] = value
                    self.write()
                    if needs_pipe:
                        self.pipe_out("PUT", item, value)
                    return(c.OK, None)
                else:
                    return(c.BAD_TYPE, None)
            except Exception as err:
                return("{} in PUT: {}".format(type(err).__name__, traceback.format_exc()), None)
        else: 
            return(c.BAD_ITEM, None)

    def DEL(self, item, needs_pipe):
        """Remove the given item from self.resource, pipe the changes to
        either DHCP or iptables, and write the changes to disk.
        
        Parameters
        ----------
        item : str
            Key of the item to be removed from the resource dict
        needs_pipe : bool
            Whether the change made needs to be piped out to DHCP or iptables

        Returns
        -------
        str
            Status code of the operation, returns negative if illegal arguments are provided
        NoneType
            Empty as DEL does not return any objects
        """

        if item in self.resource:
            try:
                self.pipe_out("DEL", item)
                del self.resource[item]
                self.write()
                return(c.OK, None)
            except Exception as err:
                return("{} in DEL: {}".format(type(err).__name__, err), None)
        else:
            return(c.BAD_ITEM, None)

    def write(self):
        """Provides write-to-disk functionality for POST, PUT and DEL.

        # TODO: Make disk-writes atomic.

        Class structures are preserved due to YAML.
        """

        with open('/caer/data/{}.yaml'.format(self.name), "w") as f:
            yaml.dump(self.resource, f)
        return

    def load(self):
        """Loads the resource from disk and instantiates it as self.resource"""
        with open('/caer/data/{}.yaml'.format(self.name), "r") as f:
            self.resource = yaml.full_load(f)
        return

        pass

    def pipe_out(self, *args):
        """Pipe the changes to either DHCP or iptables."""        
        pass
