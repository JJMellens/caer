import traceback, yaml, sys, asyncio, threading, random, time
from subprocess import Popen, PIPE
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
import data.statuscodes as c
from core.runtimevariable import RuntimeVariable


class Settings(RuntimeVariable):
    """RuntimeVariable child class represeting Caer's runtime settings.
    
    Provides functionality for modifying certain configuration settings,
    although this is currently mostly limited to debug values.
    Overrides the superclass methods POST and DEL as these are
    not permitted for this resource.

    Attributes
    ----------
    settings : dict with key name (str)
        name : str/bool/int/list 
            Contains the value of the setting

    Methods
    -------
    GET(item)
        Return the information about the given device. Inherited from superclass.
    PUT(item, value, needs_pipe)
        Update the device of name item with the information from value.
        Inherited from superclass.
    """
    

    def __init__(self, resource_name="settings"):
        super().__init__(resource_name)

    def POST(self, *args):
        """Return an error stating that the method is not available."""
        return(c.BAD_METHOD(self.name), None)

    def DEL(self, *args):
        """Return an error stating that the method is not available."""
        return(c.NO_METHOD,None)

    def create(self):
        """Writes the factory default settings to disk."""
        with open('/caer/data/factory_default.yaml', "r") as f:
            factory_default = yaml.full_load(f)
        with open('/caer/data/settings.yaml', "w") as f:
            yaml.dump(factory_default, f)
            print("Default settings created as part of first-time boot")
        return


settings = Settings()
