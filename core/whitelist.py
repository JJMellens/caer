import traceback, yaml, sys, asyncio, threading, random, time
from subprocess import Popen, PIPE
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
import data.statuscodes as c
from core.runtimevariable import RuntimeVariable


class Whitelist(RuntimeVariable):
    """RuntimeVariable child class representing the devices that
    have been whitelisted via the 2FA system.
    
    Provides functionality for adding or removing these whitelistings.
    
    Attributes
    ----------
    resource : dict with key ip (str)
        ip : float
            Timestamp of when the address was whitelisted

    Methods
    -------
    GET(item)
        Return the information about the given whitelisting.
        Inherited from superclass.

    POST(item, value, needs_pipe)
        Added a new entry to the resource dict with the intformation in value,
        keyed with the given name. Inherited from superclass.

    PUT(item, value)
        Update the value with key 'item' in the resource dict
        with the information from value. Inherited from superclass.

    DEL(item)
        Remove the value with key 'item' from the resource dict.
        Inherited from superclass.
    """


    def __init__(self, resource_name="whitelist"):
        super().__init__(resource_name)

    def create(self):
        """Create and write to disk the whitelist as an empty dictionary."""
        with open('/caer/data/whitelist.yaml', "w") as f:
            yaml.dump({}, f)
            print("Empty whitelist generated as part of first-time boot")
        return

    def pipe_out(self, method, ip):
        """Add the 2FA exception to iptables. 
        
        Note that the rule is inserted at position three, so as to not
        override the block on SSH/FTP/telnet.
        
        PARAMETERS
        ----------
        method : str
            The name of the method calling the pipe; POST or DEL
        ip : str
            The ip address to be whitelisted
        """
        if method == "DEL":
            Popen(['iptables', '-D', 'FORWARD', '-s', ip, '-j', 'ACCEPT']).communicate()
            print('{} has been removed from the whitelist'.format(ip))
        elif method == "POST":
            Popen(['iptables', '-I', 'FORWARD', '3', '-s', ip, '-j', 'ACCEPT']).communicate()
            print('{} has been added to the whitelist'.format(ip))


whitelist = Whitelist()
