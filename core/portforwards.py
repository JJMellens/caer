import traceback, yaml, sys, asyncio, threading, random, time
from subprocess import Popen, PIPE
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA256
import data.statuscodes as c
from core.runtimevariable import RuntimeVariable

class PortForwards(RuntimeVariable):
    """RuntimeVariables child object representing the ports Caer needs to forward.

    Provides functionality for adding portforwarding rules to Caer's routing
    and pipes these out to iptables. Overrides the PUT and DEL methods as
    both need to maintain information about the old rule before piping it
    to iptables.
    
    Attributes
    ----------
    name : str
        Name of the resource
    forwarded_ports : dict with key rule_name (str)
        rule_name : dict
            start_port : str
            end_port : str
            destination_ip : str
            active : bool
            protocols : list[str]
        Contains the details for each rule.

    Methods
    -------
    GET(item)
        Return the information about the given device. Inherited from superclass.

    POST(item, value, needs_pipe)
        Add a new entry to the resource dict with the information in value,
        keyed with the given name.

    PUT(item, value, needs_pipe)
        Update the device of name item with the information from value.

    DEL(item)
        Remove the device of name item from the resource dict.
    """

    
    def __init__(self, resource_name="forwarded_ports"):
        super().__init__(resource_name)

    def PUT(self, item, value, needs_pipe):
        """Update the device of name item with the information from value.

        Overrides the superclass method as it addtionally maintains
        information about the old rule needed for the iptables pipe.
        """

        if item in self.resource:
            try:
                if type(self.resource[item]) == type(value):
                    old_rule = self.resource[item].copy()
                    if type(value) == dict:
                        for key in value:
                            if value[key] is not None:
                                self.resource[item][key] = value[key]
                    else:
                        self.resource[item] = value
                    self.pipe_out("PUT", old_rule, self.resource[item])
                    self.write()
                    return(c.OK, None)
                else:
                    return(c.BAD_TYPE, None)
            except Exception as err:
                return("{} in PUT: {}".format(type(err).__name__, traceback.format_exc()), None)

    def DEL(self, item, needs_pipe):
        """Remove the rule of name item from the resource dict.

        Overrides the superclass method as it addtionally maintains
        information about the old rule needed for the iptables pipe.
        
        Parameters
        ----------
        item : str
            Key of the item to be removed from the resource dict
        needs_pipe : bool
            Whether the change made needs to be piped out to DHCP or iptables

        Returns
        -------
        str
            Status code of the operation, returns negative if illegal arguments are provided
        NoneType
            Empty as DEL does not return any objects
        """

        if item in self.resource:
            try:
                old_item = self.resource[item].copy()
                del self.resource[item]
                self.pipe_out("DEL", old_item)
                self.write()
                return(c.OK, None)
            except Exception as err:
                return("{} in DEL: {}".format(type(err).__name__, err), None)
        else:
            return(c.BAD_ITEM, None)

    def create(self):
        """Create and write to disk the forwarded_ports dict as an empty dictionary."""
        with open('/caer/data/forwarded_ports.yaml', "w") as f:
            yaml.dump({}, f)
            print("Sample port-forwarding list created as part of first-time boot")
        return

    def pipe_out(self, method, *rules):
        """Update the iptables ruleset whenever the forwarded_ports resources is modified.

        Parameters
        ---------
        method : str
            The method that called the update function
        rules : str/dict
            POST supplies the rulename, but PUT and DEL supply the full item
        """        
        if method == "DEL":
            self.pipe_helper("D", rules[0])
        elif method == "POST":
            self.pipe_helper("I", forwarded_ports.resource[rules[0]])
            pass
        elif method == "PUT":
            self.pipe_helper("D", rule = rules[0])
            self.pipe_helper("I", rules[1])
            pass
        print('Succesfully piped rule change for {} to iptables'.format(rules[0]))

    def pipe_helper(self, action, rule):
        """Write given port forwarding rules to iptables.

        TODO: fix line width
        
        Parameters
        ----------
        action : str
            Whether the rule should be inserted or deleted. Legal values are "I" or "D"
        rule : dict
            Rule to be added to- or removed from iptables
        """        
        start_port = rule["start_port"]
        end_port = rule["end_port"]
        dest_ip = rule["destination_ip"]

        for proto in rule["protocols"]: # Ensure there is a rule for all given protocols
            if start_port == end_port: # Use a single port
                    dport = start_port
                    ipport = start_port
            else: # Combine start & end port into the right 'range' format for the iptables calls
                dport = "{}:{}".format(start_port, end_port)
                ipport = "{}-{}".format(start_port, end_port)
            
            # Check if iptables rule already exists
            rule_check = Popen(['iptables', '-{}'.format(action), 'PREROUTING', '-t', 'nat', '-i', 'eth0', '-p', proto,
                        '--dport', dport, '-j', 'DNAT', '--to', '{}:{}'.format(dest_ip, ipport)], stdin=PIPE, stdout=PIPE, stderr=PIPE)
            rule_nonexistant = rule_check.communicate()[1]
            
            if (rule["active"] == True and rule_nonexistant) or action == "D": # Prevent inactive or duplicate rules from being applied
                Popen(['iptables', '-{}'.format(action), 'PREROUTING', '-t', 'nat', '-i', 'eth0', '-p', proto,
                        '--dport', dport, '-j', 'DNAT', '--to', '{}:{}'.format(dest_ip, ipport)],
                            stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()
        return
        

forwarded_ports = PortForwards()
