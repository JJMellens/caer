from core.settings import settings
from core.security import security
from core.whitelist import whitelist
from core.portforwards import forwarded_ports
from core.devices import devices
from pipes import WhitelistPipe, TwoFactorServer, DHCPPipe, UserControlServer

# Start thread for removing expired 2FA whitelistings
WhitelistPipeThread = WhitelistPipe()
WhitelistPipeThread.start()

# Start server listening for incoming 2FA messages
TwoFactorServerThread = TwoFactorServer()
TwoFactorServerThread.start()

# Start thread for detecting new DHCP clients
DHCPPipeThread = DHCPPipe()
DHCPPipeThread.start()

# Start async server listening for incoming local clients
# that want to access Caer's configuration options.
UserControlServerThread = UserControlServer()
UserControlServerThread.run()


